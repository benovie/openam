FROM tomcat:6
MAINTAINER Benjamin Heek <bheek@zigwebsoftware.nl>

COPY server.war /usr/local/tomcat/webapps/openam.war

# run tomcat
CMD /usr/local/tomcat/bin/startup.sh && wait && tail -f /usr/local/tomcat/logs/catalina.out